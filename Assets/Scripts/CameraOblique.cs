using UnityEngine;
using System.Collections;

public class CameraOblique : MonoBehaviour {
	public float horizontalOblique;
	public float verticalOblique;
	
	void Start () {
		Matrix4x4 mat = camera.projectionMatrix;
		mat[0, 2] = horizontalOblique;
		mat[1, 2] = verticalOblique;
		
		camera.projectionMatrix = mat;
	}
}
