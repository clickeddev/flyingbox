using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public float minScale;
	public float maxScale;
	public float scalingSpeed;
	
	private Transform _transform;
	private int _score;

	void Start () {
		_transform = GetComponent<Transform>();
		_score = 0;
	}
	
	void Update () {
		float scaleControl = Input.GetAxis("Vertical");
		if (scaleControl != 0) {
			Vector3 scale = _transform.localScale + 
							(Vector3.right + Vector3.up) * scalingSpeed * scaleControl * Time.deltaTime;
			if (minScale < scale.x && scale.x < maxScale) {
				_transform.localScale = scale;
			}
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.name.Equals("Resistance(Clone)")) {
			_score -= 1;
		}
		else if (collision.gameObject.name.Equals("Point(Clone)")) {
			_score += 1;
			collision.gameObject.GetComponent<Block>().Deactivate();
		}
	}
	
	void OnGUI() {
		GUI.Label(new Rect(10, 10, 300, 50), "Score : " + _score);
	}
}
