using UnityEngine;
using System.Collections;

public class StageComposer : MonoBehaviour {
	private const int DefaultPointsCount = 60;
	private const int DefaultResistancesCount = 120;
	
	private BlockPool _resistancePool;
	private BlockPool _pointPool;
	
	// TODO only for test
	private float _lastCompositionTime;
	private float _compositionInterval;
	
	public static Vector3 InvisiblePosition = new Vector3(0, 0, 1000);
	
	public GameObject point;
	public GameObject resistance;
	
	private void initVariables()
	{
		_resistancePool = GameObject.Find("ResistancePool").GetComponent<BlockPool>();
		_pointPool = GameObject.Find("PointPool").GetComponent<BlockPool>();
		
		_compositionInterval = 1.0f;
	}
	
	void Awake() {
		initVariables();
	}
	
	void Start() {
		_lastCompositionTime = Time.time;
	}
	
	void Update() {
		if (Time.time - _lastCompositionTime > _compositionInterval) {
			// compose new obstacle
			Block block = _resistancePool.Retain(new Vector3(100, 5, 50));
			block.Activate();
			
			block = _pointPool.Retain(new Vector3(100, -5, 50));
			block.Activate();
			
			_lastCompositionTime = Time.time;
		}
	}
}
