using UnityEngine;
using System.Collections;

public class GameObjectPool : MonoBehaviour {
	private ArrayList _used;
	private ArrayList _freed;
	
	public GameObjectPool()
	{
		_used = new ArrayList();
		_freed = new ArrayList();
	}
	
	public void Add(GameObject obj)
	{
		_freed.Add(obj);
	}
	
	public GameObject Retain() 
	{
		if (_freed.Count == 0) {
			return null;
		}
		
		GameObject result = (GameObject)_freed[0];
		_freed.RemoveAt(0);
		_used.Add(result);
		
		return result;
	}
	
	public bool Release(GameObject obj) 
	{
		if (_used.Contains(obj) == false) {
			return false;
		}
		
		_used.Remove(obj);
		_freed.Add(obj);
		return true;
	}
}
