using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {
	private BlockPool _owner;
	
	public float speed;
	
	public void SetOwner(BlockPool owner)
	{
		_owner = owner;
	}
	
	public void Activate() {
		renderer.enabled = true;
		rigidbody.detectCollisions = true;
	}
	
	public void Deactivate() {
		renderer.enabled = false;
		rigidbody.detectCollisions = false;
		rigidbody.angularVelocity = Vector3.zero;
		rigidbody.velocity = Vector3.zero;
		transform.position = StageComposer.InvisiblePosition;
		transform.rotation = Quaternion.identity;
		
		_owner.Release(this.gameObject);
	}
	
	void Start () {
		Deactivate();
	}

	void Update () {
		if (renderer.enabled) {
			transform.position += Vector3.left * speed * Time.deltaTime;
			
			if (transform.position.x < -150) {
				Deactivate();
			}
		}
	}
}
