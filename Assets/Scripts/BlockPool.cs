using UnityEngine;
using System.Collections;

public class BlockPool : GameObjectPool {
	public GameObject block;
	public int defaultBlockCount;
	
	private GameObject RequestAdditionalBlock()
	{
		GameObject additional = (GameObject)Instantiate(block, StageComposer.InvisiblePosition, Quaternion.identity);
		additional.GetComponent<Block>().SetOwner(this);
		Add(additional);
		
		return additional;
	}
	
	void Awake()
	{
		for (int i = 0; i < defaultBlockCount; i++) {
			RequestAdditionalBlock();
		}
	}
	
	public Block Retain(Vector3 position)
	{
		GameObject obj = Retain();
		if (obj == null) {
			obj = RequestAdditionalBlock();
		}
		obj.transform.position = position;
		
		return obj.GetComponent<Block>();
	}
}
